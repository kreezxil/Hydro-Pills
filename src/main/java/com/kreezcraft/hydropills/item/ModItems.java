package com.kreezcraft.hydropills.item;

import com.kreezcraft.hydropills.HydroPillsMod;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;


public class ModItems
{

    public static final Item HYDRO_PILL = registerItem("hydro_pill", new Item(new FabricItemSettings().group(ItemGroup.FOOD)));

    public static Item registerItem(String name, Item item) {
        return Registry.register(Registry.ITEM, new Identifier(HydroPillsMod.MOD_ID, name), item);
    }

    public static void registerModItems() {
        System.out.println("Registering Mod Items for " + HydroPillsMod.MOD_ID);
    }

}
